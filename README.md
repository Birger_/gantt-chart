# Gantt chart

The file `plot_gantt.R` contains the function `plot_gantt` which creates
a simple gantt chart.

```
source("plot_gantt.R")

# create data frame
df <- tibble::tribble(
  ~start, ~end, ~name,
  "2018-09-01", "2018-11-30", "Task 1", 
  "2018-11-01", "2018-12-13", "Task 2",           
  "2018-10-08", "2018-10-12", "Task 3",
  "2018-12-03", "2019-01-11", "Task 4"
) 

title = "Project name"
subtitle = paste("Gantt Chart from", Sys.Date())
plot_gantt(df, title, subtitle)
```

![](gantt_plot.png)
